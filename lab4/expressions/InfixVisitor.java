package expression_package;

import java.io.PrintStream;

import expression_package.*;

public class InfixVisitor implements ExpressionsVisitor{

    protected PrintStream out;

    public InfixVisitor (PrintStream o){
        out = o;
    }

    protected StringBuffer printChild ( SimpleNode node, StringBuffer data){
        Token t1 = node.jjtGetFirstToken();
        Token t = new Token();
        t.next = t1;
        
        SimpleNode n;
        for (int i = 0; i < node.jjtGetNumChildren(); i++) {
            n = (SimpleNode)node.jjtGetChild(i);
            while (true) {
                t = t.next;
                if (t == n.jjtGetFirstToken()) break;
                data.append(t);
            }
            n.jjtAccept(this, data);
            t = n.jjtGetLastToken();
        }
        
        while (t != node.jjtGetLastToken()) {
            t = t.next;
            data.append(t);
        }
        return data;
    }


    public StringBuffer visit(SimpleNode node, StringBuffer data){
        return printChild(node, data);   
    }
    
    public StringBuffer visit(ASTExp node, StringBuffer data){
        return printChild(node, data);     
    }

    public StringBuffer visit(ASTExpRest node, StringBuffer data){
        return printChild(node, data);
    }

    public StringBuffer visit(ASTTerm node, StringBuffer data){
        return printChild(node, data);    
    }

    public StringBuffer visit(ASTTermRest node, StringBuffer data){
        return printChild(node, data);    
    }

    public StringBuffer visit(ASTFactor node, StringBuffer data){
        return printChild(node, data);    
    }

}