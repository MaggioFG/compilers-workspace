package expression_package;

import java.io.PrintStream;
import java.util.Stack;
import java.util.*;

import expression_package.*;

public class PrefixVisitor implements ExpressionsVisitor {

    protected PrintStream out;

    protected boolean isInteger(Object value) {
        try {
            Integer.parseInt(value.toString());
        } catch (Exception nfe) {
            return false;
        }
        return true;
    }

   


    public PrefixVisitor(PrintStream o) {
        out = o;
    }

    public StringBuffer printPrefix(SimpleNode node, StringBuffer data) {
        Token t1 = node.jjtGetFirstToken();
        Token t = new Token();
        Token previous = new Token();
        Stack<String> operatorStack = new Stack<String>();
        String output = new String();
        boolean found_operator = false;
        t.next = t1;
        String infix = new String();

        do {
            t = t.next;
            infix += t.image;
        } while (t != node.jjtGetLastToken());

        operatorStack.push(")");
        infix += "(";
        for (int i = infix.length() - 1; i >= 0; i--) {
            if (isInteger(infix.charAt(i)))
                output += infix.charAt(i);
            if (infix.charAt(i) == ")".charAt(0))
                operatorStack.push(")");
            if (infix.charAt(i) == "+".charAt(0)) {
                if (!operatorStack.empty()) {
                    try {
                        while (operatorStack.peek().equals("*")) {
                            output += operatorStack.pop();
                            if (operatorStack.empty())
                                break;
                        }

                    } catch (EmptyStackException ese) {
                        out.print("\n Empty Stack Exception \n");
                        ese.printStackTrace();
                    }
                }
                operatorStack.push("+");
            }
            if (infix.charAt(i) == "*".charAt(0))
                operatorStack.push("*");
            if (infix.charAt(i) == "(".charAt(0)) {
                if (!operatorStack.empty()) {
                    while (!operatorStack.peek().equals(")")) {
                        output += operatorStack.pop();
                        if (operatorStack.empty())
                            break;
                    }
                    operatorStack.pop();
                }
            }
        }
        while (!operatorStack.empty()) {
            output += operatorStack.pop();
        }
        StringBuilder b = new StringBuilder(output);

        data.append(b.reverse());
        return data;
    }

    public StringBuffer visit(SimpleNode node, StringBuffer data) {
        return printPrefix(node, data);
    }

    public StringBuffer visit(ASTExp node, StringBuffer data) {
        return printPrefix(node, data);
    }

    public StringBuffer visit(ASTExpRest node, StringBuffer data) {
        return printPrefix(node, data);
    }

    public StringBuffer visit(ASTTerm node, StringBuffer data) {
        return printPrefix(node, data);
    }

    public StringBuffer visit(ASTTermRest node, StringBuffer data) {
        return printPrefix(node, data);
    }

    public StringBuffer visit(ASTFactor node, StringBuffer data) {
        return printPrefix(node, data);
    }

}