package expression_package;

import java.io.PrintStream;
import java.util.Stack;

import expression_package.*;

public class EvaluationVisitor implements ExpressionsVisitor {

    private static String prefixExpression;

    protected PrintStream out;

    public EvaluationVisitor(PrintStream o) {
        out = o;
    }

    protected boolean isInteger(Object value) {
        try {
            Integer.parseInt(value.toString());
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    protected boolean isOperator(char c) {
        char mul = '*';
        char add = '+';
        if (c == add || c == mul)
            return true;
        return false;
    }

    protected StringBuffer evaluatePrefix(SimpleNode node, StringBuffer data) {
        Token t1 = node.jjtGetFirstToken();
        Token t = new Token();
        t.next = t1;
        Stack<String> stack = new Stack<String>();

        for (int i = prefixExpression.length() - 1; i >= 0; i--) {
            char c = prefixExpression.charAt(i);

            if (isInteger(c)) {
                stack.push(String.valueOf(c));
            }

            if (isOperator(c)) {
                try {
                    int value1 = Integer.parseInt(stack.pop());
                    int value2 = Integer.parseInt(stack.pop());
                    if (c == "*".charAt(0)) {
                        stack.push(String.valueOf(value1 * value2));
                    } else if (c == "+".charAt(0)) {
                        stack.push(String.valueOf(value1 + value2));
                    }

                } catch (NumberFormatException nfe) {
                    nfe.printStackTrace();
                }
            }
        }

        data.append(stack.pop());

        return data;
    }

    public StringBuffer visit(SimpleNode node, StringBuffer data) {
        return evaluatePrefix(node, data);
    }

    public StringBuffer visit(ASTExp node, StringBuffer data) {
        ExpressionsVisitor prefixVisitor = new PrefixVisitor(out);
        StringBuffer prefixExpressionBuffer = new StringBuffer();
        node.jjtAccept(prefixVisitor, prefixExpressionBuffer);
        prefixExpression = prefixExpressionBuffer.toString();

        return evaluatePrefix(node, data);
    }

    public StringBuffer visit(ASTExpRest node, StringBuffer data) {
        return evaluatePrefix(node, data);
    }

    public StringBuffer visit(ASTTerm node, StringBuffer data) {
        return evaluatePrefix(node, data);
    }

    public StringBuffer visit(ASTTermRest node, StringBuffer data) {
        return evaluatePrefix(node, data);
    }

    public StringBuffer visit(ASTFactor node, StringBuffer data) {
        return evaluatePrefix(node, data);
    }

}