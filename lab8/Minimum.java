public class Minimum
{
    public static void main(String[] args)
    {
        int X, Y, Z;
	    X = Integer.parseInt(args[0]);
	    Y = Integer.parseInt(args[1]);
	    Z = Min(X, Y);
	    System.out.println(Z);
    }

    public static int Min(int U, int V)
    {
        int T;
	    if (U < V) T = U;
	    else T = V;
	    return T;
    }
}
    