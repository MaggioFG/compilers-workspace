/* Generated By:JJTree: Do not edit this line. ASTExpRest.java Version 4.3 */
/* JavaCCOptions:MULTI=true,NODE_USES_PARSER=true,VISITOR=true,TRACK_TOKENS=true,NODE_PREFIX=AST,NODE_EXTENDS=,NODE_FACTORY=*,SUPPORT_CLASS_VISIBILITY_PUBLIC=true */
package minijava_package;

public
class ASTExpRest extends SimpleNode {
  public ASTExpRest(int id) {
    super(id);
  }

  public ASTExpRest(MiniJava p, int id) {
    super(p, id);
  }

  public static Node jjtCreate(int id) {
    return new ASTExpRest(id);
  }

  public static Node jjtCreate(MiniJava p, int id) {
    return new ASTExpRest(p, id);
  }

  /** Accept the visitor. **/
  public Object jjtAccept(MiniJavaVisitor visitor, String data) {
    return visitor.visit(this, data);
  }
}
/* JavaCC - OriginalChecksum=955ad6d2062af1794925072416e1046b (do not edit this line) */
