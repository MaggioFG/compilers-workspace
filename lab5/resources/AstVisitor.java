package minijava_package;

import minijava_package.*;

import java.io.PrintStream;

public class AstVisitor implements MiniJavaVisitor {

    protected PrintStream out;

    public AstVisitor(PrintStream o) {
        out = o;
    }

    protected Object printChild(SimpleNode node, String data) {
        node.dump(data);

        return data;
    }

    /*
     * 
     * boolean isRoot = false; depth++;
     * 
     * try { numSiblings = node.jjtGetParent().jjtGetNumChildren();
     * 
     * } catch (NullPointerException npe) { isRoot = true; } if (isRoot) {
     * out.print(node); out.print("\n--"); isRoot = false; }
     * 
     * SimpleNode n; for (int i = 0; i < node.jjtGetNumChildren(); i++) { n =
     * (SimpleNode) node.jjtGetChild(i); if (i == node.jjtGetNumChildren() - 1) {
     * out.print(n); out.print("\n"); for (int j=0; i<depth ;i++) out.print("--");
     * }else{ out.print(n); out.print("  "); } n.jjtAccept(this, i); depth--; }
     * return data;
     */

    public Object visit(SimpleNode node, String data) {
        return printChild(node, data);
    }

    public Object visit(ASTProgram node, String data) {
        return printChild(node, data);
    }

    public Object visit(ASTMainClass node, String data) {
        return printChild(node, data);
    }

    public Object visit(ASTClassDecl node, String data) {
        return printChild(node, data);
    }

    public Object visit(ASTVarDecl node, String data) {
        return printChild(node, data);
    }

    public Object visit(ASTMethodDecl node, String data) {
        return printChild(node, data);
    }

    public Object visit(ASTFormalList node, String data) {
        return printChild(node, data);
    }

    public Object visit(ASTStatement node, String data) {
        return printChild(node, data);
    }

    public Object visit(ASTExp node, String data) {
        return printChild(node, data);
    }

    public Object visit(ASTExpRest node, String data) {
        return printChild(node, data);
    }

    public Object visit(ASTTerm node, String data) {
        return printChild(node, data);
    }

    public Object visit(ASTTermRest node, String data) {
        return printChild(node, data);
    }

    public Object visit(ASTExpList node, String data) {
        return printChild(node, data);
    }

}