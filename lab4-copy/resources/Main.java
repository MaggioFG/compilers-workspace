package expression_package;

import java.io.*;

  
  /*@egen*/

public class Main
{

    public static void main (String args[]) throws ParseException{
        System.out.println("Reading from standard input ...");
        Expressions parserTree = new Expressions(System.in);
        try {
            ASTExp node = parserTree.Exp();
            System.out.println(node.jjtGetNumChildren());
            
            System.out.println(jjtree.rootNode());

            node.dump(">");
            System.out.println("\n Thank you");
        } catch (Exception e) {
            System.out.println("\n Something went wrong \n");
            System.err.println(e.getMessage());
            e.printStackTrace();
        }
    }
}
  /* public static void main(String args[]) {
    System.err.println("Reading from standard input...");
    Expressions p = new Expressions(System.in);
    try {
      ASTExp cu = p.Exp();
      //ExpressionsVisitor visitor = new AddAcceptVisitor(System.out);
      cu.jjtAccept(visitor, null);
      System.err.println("Thank you.");
    } catch (Exception e) {
      System.err.println("Oops.");
      System.err.println(e.getMessage());
      e.printStackTrace(); */

