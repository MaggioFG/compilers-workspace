package expression_package;

import java.io.PrintStream;

import expression_package.*;

public class InfixVisitor implements ExpressionsVisitor{

    private static int count;

    protected PrintStream out;

    public InfixVisitor (PrintStream o){
        out = o;
        count = 0;
    }

    protected StringBuffer printChild ( SimpleNode node, StringBuffer data){
        Token t1 = node.jjtGetFirstToken();
        Token t = new Token();
        t.next = t1;
        
        SimpleNode n;
        for (int ord = 0; ord < node.jjtGetNumChildren(); ord++) {
            n = (SimpleNode)node.jjtGetChild(ord);
            while (true) {
                t = t.next;
                if (t == n.jjtGetFirstToken()) break;
                data.append(t);
            }
            n.jjtAccept(this, data);
            t = n.jjtGetLastToken();
        }
        
        while (t != node.jjtGetLastToken()) {
            t = t.next;
            data.append(t);
        }
        return data;
    }


        /* 
        if (node.jjtGetNumChildren() == 0){
            data.append(node.jjtGetFirstToken());        
        }
        
        for( int i = 0; i < node.jjtGetNumChildren(); i++){
            node.jjtGetChild(i).jjtAccept(this, data);
            if (node.jjtGetNumChildren() == 0){
                data.append(node.jjtGetFirstToken());        
            }
            //data.append("  ");
        }
        count++;
        //data.append(" " + count);
        //data.append("\n");
 */  


    public StringBuffer visit(SimpleNode node, StringBuffer data){
        

        //out.print( (Integer.parseInt(node.jjtGetFirstToken().image))*2 );
        return printChild(node, data);
        
    }
    public StringBuffer visit(ASTExp node, StringBuffer data){
        

        return printChild(node, data);
        /* StringBuffer stringa = new StringBuffer(node.jjtGetFirstToken().image) ;
        for( int i = 0; i < node.jjtGetNumChildren(); i++){
            stringa.append(node.jjtGetChild(i).jjtAccept(this, null));
        }
        
        
        //out.print(stringa);
        //out.print( node.jjtGetChild(0) );
        ////out.print( (Integer.parseInt(node.jjtGetFirstToken().image))*2 );
        return stringa.toString(); */
        
    }
    public StringBuffer visit(ASTExpRest node, StringBuffer data){
        //data.append(node.jjtGetFirstToken());
        //out.print(node.jjtGetFirstToken());
        //data.append(node.jjtGetFirstToken().image);
        return printChild(node, data);
        
    }
    /* public StringBuffer visit(ASTAdd node, StringBuffer data){
        
        //out.print(node.jjtGetFirstToken());
        return printChild(node, data);
        
    } */
    public StringBuffer visit(ASTTerm node, StringBuffer data){
        
        //out.print(node.jjtGetFirstToken());
        return printChild(node, data);
        
    }
    public StringBuffer visit(ASTTermRest node, StringBuffer data){
        out.print("Term REST");
        //data.append(node.jjtGetFirstToken().image);
        //out.print(node.jjtGetFirstToken());
        return printChild(node, data);
        
    }
    public StringBuffer visit(ASTFactor node, StringBuffer data){
        //data.append(node.jjtGetFirstToken());
        //out.print(node.jjtGetFirstToken());
        return printChild(node, data);
        
    }
   /*  public StringBuffer visit(ASTnumber node, StringBuffer data){
        //data.append(node.jjtGetFirstToken());
        //out.print(node.jjtGetFirstToken());
        return printChild(node, data);    
    }
 */

}